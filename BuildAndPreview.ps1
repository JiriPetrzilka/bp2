﻿Param([switch]$NoPreview)

pwd | Push-Location 
cd 'C:\Users\Jiří Petržilka\AppData\Local\Programs\MiKTeX 2.9\miktex\bin\x64'

pdflatex.exe -include-directory=D:\FJFI\BP2\bp2\Text -output-directory=D:\FJFI\BP2\bp2\Text -interaction=nonstopmode -file-line-error bp.tex

if($? -and !$NoPreview)
{
    D:\FJFI\BP2\bp2\Text\bp.pdf
}

Pop-Location | cd