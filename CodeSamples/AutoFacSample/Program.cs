﻿using System.Reflection;
using Autofac;
using NLog;
using SampleLibrary;

namespace AutofacSample
{
    class Program
    {
        static void Main()
        {
            var sampleAssembly = Assembly
                .GetAssembly(typeof(IRunner));
            var builder = new ContainerBuilder();

            //single component registration
            builder
                .Register(c => LogManager.GetCurrentClassLogger())
                .As<ILogger>();

            //mass registration via assembly scanning
            builder
                .RegisterAssemblyTypes(sampleAssembly)
                .AsImplementedInterfaces()
                .InstancePerDependency();

            //resolve component root from container
            var container = builder.Build();
            var runner = container.Resolve<IRunner>();

            runner.Run();
        }
    }
}
