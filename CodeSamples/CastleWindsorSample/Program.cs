﻿using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NLog;
using SampleLibrary;

namespace CastleWindsorSample
{
    class Program
    {
        static void Main()
        {
            var sampleAssembly = Assembly
                .GetAssembly(typeof(IRunner));
            var container = new WindsorContainer();

            //single component registration
            container.Register(Component
                .For<ILogger>()
                .Instance(LogManager.GetCurrentClassLogger()));

            //mass registration via assembly scanning
            container.Register(Classes
                .FromAssembly(sampleAssembly)
                .Pick()
                .WithServiceDefaultInterfaces()
                .LifestyleTransient());

            //resolve component root from container
            var svc = container.Resolve<IRunner>();

            svc.Run();
        }
    }
}
