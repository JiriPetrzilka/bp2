﻿using System.Linq;
using System.Reflection;
using NLog;
using SampleLibrary;
using SimpleInjector;

namespace SimpleInjectorSample
{
    class Program
    {
        static void Main()
        {
            var container = new Container();
            var sampleAssembly = Assembly
                .GetAssembly(typeof(IRunner));

            //single component registration
            container.Register<ILogger>(
                () => LogManager.GetCurrentClassLogger(),
                Lifestyle.Transient);

            //mass registration using reflection
            sampleAssembly.GetExportedTypes()
                .Where(t => t.GetInterfaces().Any())
                .ToList()
                .ForEach(type =>
                    container.Register(
                        type.GetInterfaces().Single(),
                        type,
                        Lifestyle.Transient));

            //closing the container
            container.Verify();

            //resolve component root from container
            var runner = container.GetInstance<IRunner>();

            runner.Run();
        }
    }
}
