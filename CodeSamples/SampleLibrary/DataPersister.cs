﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SampleLibrary.Configuration;
using SampleLibrary.Model;

namespace SampleLibrary
{
    public class DataPersister : IDataPersister
    {
        readonly IConfig config;

        public DataPersister(IConfig config) => this.config = config;

        public void SaveVatData(Data vatData)
        {
            var jsonData = JsonConvert.SerializeObject(vatData, Formatting.Indented);
            File.WriteAllText(config.StoragePath, jsonData);
        }

        public Data LoadVatData()
        {
            if (!File.Exists(config.StoragePath))
                return null;

            var rawData = File.ReadAllText(config.StoragePath);
            return JsonConvert.DeserializeObject<Data>(rawData);
        }
    }
}
