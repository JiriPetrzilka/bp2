﻿using SampleLibrary.Model;

namespace SampleLibrary
{
    public interface IServiceProxy
    {
        Data GetVatRates();
    }
}
