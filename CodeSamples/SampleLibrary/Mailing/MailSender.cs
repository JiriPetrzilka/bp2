﻿using NLog;
using SampleLibrary.Configuration;
using SampleLibrary.Model;

namespace SampleLibrary.Mailing
{
    public class MailSender : IMailSender
    {
        readonly IConfig config;
        readonly ILogger logger;

        public MailSender(IConfig config, ILogger logger)
        {
            this.config = config;
            this.logger = logger;
        }

        public void SendAlertMail(Data orignalState, Data currentState)
        {
            //mail sending logic
            //uses values from "config" dependency for mailing setup

            logger.Log(LogLevel.Info, "Alert mail sent.");
        }
    }
}
