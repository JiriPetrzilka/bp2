﻿using SampleLibrary.Model;

namespace SampleLibrary.Mailing
{
    public interface IMailSender
    {
        void SendAlertMail(Data orignalState, Data currentState);
    }
}
