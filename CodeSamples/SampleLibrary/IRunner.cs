﻿using NLog;
using SampleLibrary.Mailing;

namespace SampleLibrary
{
    public interface IRunner
    {
        void Run();
    }
}
