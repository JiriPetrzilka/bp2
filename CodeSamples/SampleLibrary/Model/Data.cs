﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary.Model
{
    public class Data
    {
        public IEnumerable<VatRate> VatRates { get; set; }
        public DateTime StatusDateUtc { get; set; }
    }
}
