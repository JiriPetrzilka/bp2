﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using SampleLibrary.Configuration;
using SampleLibrary.Model;

namespace SampleLibrary
{
    public class ServiceProxy : IServiceProxy
    {
        readonly IConfig config;

        public ServiceProxy(IConfig config)
        {
            this.config = config;
        }

        public Data GetVatRates()
        {
            var client = new RestClient(config.ApiUrl);
            RestRequest request = new RestRequest("rate_list");
            request.AddParameter("access_key", config.ApiKey);

            var response = client.Execute(request);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(
                    String.Format("Api error.\nResponse Code{0}\nDetails {1}",
                        response.StatusCode,
                        response.Content ?? String.Empty));

            var rawData = JObject.Parse(response.Content);

            return ParseData(rawData);
        }

        Data ParseData(JObject rawData)
        {
            if (!rawData["success"].Value<bool>())
                throw new Exception("Failure on server side.");

            return new Data
            {
                StatusDateUtc = DateTime.UtcNow,
                VatRates = rawData["rates"]
                    .Children()
                    .Select(item =>
                        new VatRate
                        {
                            Country = item.Path.Substring(6),
                            Rate = item
                                .Children()["standard_rate"]
                                .First()
                                .Value<decimal>()
                        })
                        .ToArray()
            };
        }
    }
}
