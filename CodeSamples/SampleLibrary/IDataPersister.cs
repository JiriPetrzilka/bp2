﻿using SampleLibrary.Model;

namespace SampleLibrary
{
    public interface IDataPersister
    {
        Data LoadVatData();
        void SaveVatData(Data vatData);
    }
}
