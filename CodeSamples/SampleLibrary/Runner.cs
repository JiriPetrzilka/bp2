﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using SampleLibrary.Configuration;
using SampleLibrary.Mailing;

namespace SampleLibrary
{
    public class Runner : IRunner
    {
        readonly ILogger logger;
        readonly IMailSender mailSender;
        readonly IServiceProxy serviceProxy;
        readonly IDataPersister dataPersister;

        public Runner(
            ILogger logger,
            IMailSender mailSender,
            IServiceProxy serviceProxy,
            IDataPersister dataPersister)
        {
            this.logger = logger;
            this.mailSender = mailSender;
            this.serviceProxy = serviceProxy;
            this.dataPersister = dataPersister;
        }

        public void Run()
        {
            var oldVatData = dataPersister.LoadVatData();
            var newVatData = serviceProxy.GetVatRates();
            bool ratesChanged = oldVatData == null ||
                newVatData
                .VatRates
                .Join(
                    oldVatData.VatRates,
                    x => x.Country,
                    y => y.Country,
                    (oldItem, newItem) => new
                    {
                        Code = oldItem.Country,
                        OldRate = oldItem?.Rate,
                        NewRate = newItem?.Rate
                    })
                .Any(z => z.OldRate != z.NewRate);
            
            if (ratesChanged)
            {
                logger.Log(LogLevel.Info, "VAT rates have changed.");
                mailSender.SendAlertMail(oldVatData, newVatData);
            }
            else
                logger.Log(LogLevel.Info, "No change in VAT rates.");

            dataPersister.SaveVatData(newVatData);
        }
    }
}
