﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace SampleLibrary.Configuration
{
    public class Config : IConfig
    {
        public Config()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.GetFullPath("Configuration"))
                .AddJsonFile("config.json", optional: false, reloadOnChange: true);
            var configuration = builder.Build();

            configuration.Bind(this);
        }

        public string ApiUrl { get; set; }
        public string ApiKey { get; set; }
        public string StoragePath { get; set; }
        public string SmtpServer { get; set; }
        public string Sender { get; set; }
        public string Recipients { get; set; }
    }
}
