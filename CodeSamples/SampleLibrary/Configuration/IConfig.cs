﻿using System.Collections.Generic;

namespace SampleLibrary.Configuration
{
    public interface IConfig
    {
        string ApiKey { get; set; }
        string ApiUrl { get; set; }
        string Recipients { get; set; }
        string Sender { get; set; }
        string SmtpServer { get; set; }
        string StoragePath { get; set; }
    }
}
