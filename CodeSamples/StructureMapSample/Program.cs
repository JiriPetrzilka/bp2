﻿using NLog;
using SampleLibrary;
using StructureMap;

namespace StructureMapSample
{
    class Program
    {
        static void Main()
        {
            var container = new Container(c =>
            {
                //single component registration
                c.For<ILogger>()
                    .Use(x => LogManager.GetCurrentClassLogger())
                    .Transient();

                //mass registration using reflection
                c.Scan(asm =>
                {
                    asm.AssemblyContainingType<IRunner>();
                    asm.WithDefaultConventions();
                });
            });
            //resolve component root from container
            var runner = container.GetInstance<IRunner>();
            runner.Run();
        }
    }
}
