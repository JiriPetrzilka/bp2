﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using NSubstitute;
using SampleLibrary;
using NLog;
using SampleLibrary.Mailing;
using SampleLibrary.Model;

namespace SampleLibrary.Tests
{
    public class RunnerTests
    {
        Data CreateVatRateData(DateTime date, decimal rate1, decimal rate2)
        {
            return new Data()
            {
                StatusDateUtc = date,
                VatRates = new[]
                {
                    new VatRate ()
                    {
                        Country = "cz",
                        Rate = rate1
                    },
                    new VatRate()
                    {
                        Country = "sk",
                        Rate = rate2
                    }
                }
            };
        }

        [Fact]
        public void ShouldSendMailAndSaveNewRatesAndLogWhenVarRateChanged()
        {
            var oldVatData = CreateVatRateData(DateTime.UtcNow.AddDays(-1), 20, 15);
            var newVatData = CreateVatRateData(DateTime.UtcNow, 25, 10);
            var logger = Substitute.For<ILogger>();
            var mailSender = Substitute.For<IMailSender>();
            var serviceProxy = Substitute.For<IServiceProxy>();
            serviceProxy.GetVatRates().Returns(newVatData);
            var dataPersister = Substitute.For<IDataPersister>();
            dataPersister.LoadVatData().Returns(oldVatData);
            var sut = new Runner(logger, mailSender, serviceProxy, dataPersister);

            sut.Run();

            logger.Received().Log(LogLevel.Info, "VAT rates have changed.");
            mailSender.Received().SendAlertMail(oldVatData, newVatData);
            dataPersister.Received().SaveVatData(newVatData);
        }

        [Fact]
        public void ShouldOnlySaveNewRatesAndLogWhenRatesDidNotChangeAndRateFileExists()
        {
            var oldVatData = CreateVatRateData(DateTime.UtcNow.AddDays(-1), 20, 15);
            var newVatData = CreateVatRateData(DateTime.UtcNow, 20, 15);
            var logger = Substitute.For<ILogger>();
            var mailSender = Substitute.For<IMailSender>();
            var serviceProxy = Substitute.For<IServiceProxy>();
            serviceProxy.GetVatRates().Returns(newVatData);
            var dataPersister = Substitute.For<IDataPersister>();
            dataPersister.LoadVatData().Returns(oldVatData);
            var sut = new Runner(logger, mailSender, serviceProxy, dataPersister);

            sut.Run();

            logger.Received().Log(LogLevel.Info, "No change in VAT rates.");
            dataPersister.Received().SaveVatData(newVatData);
        }

        [Fact]
        public void ShouldOnlySaveNewRatesAndLogWhenRatesDidNotChangeAndRateFileIsMissing()
        {
            var newVatData = CreateVatRateData(DateTime.UtcNow, 20, 15);
            var logger = Substitute.For<ILogger>();
            var mailSender = Substitute.For<IMailSender>();
            var serviceProxy = Substitute.For<IServiceProxy>();
            serviceProxy.GetVatRates().Returns(newVatData);
            var dataPersister = Substitute.For<IDataPersister>();
            dataPersister.LoadVatData().Returns(c => null);
            var sut = new Runner(logger, mailSender, serviceProxy, dataPersister);

            sut.Run();

            logger.Received().Log(LogLevel.Info, "VAT rates have changed.");
            mailSender.Received().SendAlertMail(null, newVatData);
            dataPersister.Received().SaveVatData(newVatData);
        }
    }
}
