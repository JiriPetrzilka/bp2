﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleLibrary.Configuration;
using Xunit;

namespace SampleLibrary.Tests
{
    public class ConfigTests
    {
        [Fact]
        public void ShouldLoadConfig()
        {
            var sut = new Config();

            Assert.Equal("https://www.foo.com/api", sut.ApiUrl);
            Assert.Equal("FooBar", sut.ApiKey);
            Assert.Equal("C:\foo", sut.StoragePath);
            Assert.Equal("smtp.foo.com", sut.SmtpServer);
            Assert.Equal("alert@foo.com", sut.Sender);
            Assert.Equal("admin1@foo.com,admin2@foo.com", sut.Recipients);
        }
    }
}
