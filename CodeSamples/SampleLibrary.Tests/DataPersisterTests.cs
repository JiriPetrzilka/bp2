﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using SampleLibrary.Configuration;
using SampleLibrary.Model;
using Xunit;

namespace SampleLibrary.Tests
{
    public class DataPersisterTests
    {
        const string dataFilePath = "data.json";
        const string jsonData = @"{
  ""VatRates"": [
    {
      ""Country"": ""cz"",
      ""Rate"": 20.0
    },
    {
      ""Country"": ""sk"",
      ""Rate"": 30.0
    }
  ],
  ""StatusDateUtc"": ""2018-04-12T08:22:00""
}";

        [Fact]
        public void ShouldSaveData()
        {
            var data = new Data()
            {
                StatusDateUtc = new DateTime(2018, 04, 12, 8,22,00),
                VatRates = new[]
                {
                    new VatRate() { Country = "cz", Rate = 20 },
                    new VatRate() { Country = "sk", Rate = 30}
                }
            };
            var config = Substitute.For<IConfig>();
            config.StoragePath.Returns(dataFilePath);

            var sut = new DataPersister(config);
            sut.SaveVatData(data);

            Assert.Equal(jsonData, File.ReadAllText(dataFilePath));

            File.Delete(dataFilePath);
        }

        [Fact]
        public void ShouldLoadData()
        {
            var data = new Data()
            {
                StatusDateUtc = new DateTime(2018, 04, 12, 8, 22, 00),
                VatRates = new[]
                {
                    new VatRate() { Country = "cz", Rate = 20 },
                    new VatRate() { Country = "sk", Rate = 30}
                }
            };
            var config = Substitute.For<IConfig>();
            config.StoragePath.Returns(dataFilePath);
            File.WriteAllText(dataFilePath, jsonData);

            var sut = new DataPersister(config);
            var loadedData = sut.LoadVatData();

            Assert.Equal(new DateTime(2018, 04, 12, 8, 22, 00), loadedData.StatusDateUtc);
            Assert.Equal("cz", loadedData.VatRates.First().Country);
            Assert.Equal(20, loadedData.VatRates.First().Rate);
            Assert.Equal("sk", loadedData.VatRates.Last().Country);
            Assert.Equal(30, loadedData.VatRates.Last().Rate);

            File.Delete(dataFilePath);
        }

        [Fact]
        public void ShouldReturnNullWhenNoDataPresent()
        {
            if (File.Exists(dataFilePath))
                File.Delete(dataFilePath);

            var config = Substitute.For<IConfig>();
            config.StoragePath.Returns(dataFilePath);

            var sut = new DataPersister(config);
            var loadedData = sut.LoadVatData();
            Assert.Null(loadedData);
        }
    }
}
