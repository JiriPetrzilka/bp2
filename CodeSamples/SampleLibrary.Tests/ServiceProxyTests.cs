﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using SampleLibrary.Configuration;
using Xunit;

namespace SampleLibrary.Tests
{
    public class ServiceProxyTests
    {
        [Fact]
        public void ShouldGetVatRatesFromWs()
        {
            IConfig config = Substitute.For<IConfig>();
            config.ApiUrl.Returns("http://apilayer.net/api/");
            config.ApiKey.Returns("ce2a36f29952a8a77aa1d66f5764814f");
            var sut = new ServiceProxy(config);

            var rates = sut.GetVatRates();

            Assert.InRange(
                rates.StatusDateUtc,
                DateTime.UtcNow.AddSeconds(-15),
                DateTime.UtcNow.AddSeconds(15));
            Assert.NotEmpty(rates.VatRates);
        }
    }
}
