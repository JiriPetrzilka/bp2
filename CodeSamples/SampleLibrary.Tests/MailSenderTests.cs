﻿using NLog;
using NSubstitute;
using SampleLibrary.Mailing;
using Xunit;

namespace SampleLibrary.Tests
{
    public class MailSenderTests
    {
        [Fact]
        public void ShouldLog()
        {
            var logger = Substitute.For<ILogger>();
            var config = Substitute.For<Configuration.IConfig>();
            var sut = new MailSender(config, logger);

            sut.SendAlertMail(null, null);
            logger.Received().Log(LogLevel.Info, "Alert mail sent.");
        }
    }
}
