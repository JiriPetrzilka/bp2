﻿using Ninject;
using Ninject.Extensions.Conventions;
using NLog;
using SampleLibrary;

namespace NinjectSample
{
    class Program
    {
        static void Main()
        {
            var kernel = new StandardKernel();

            //single component registration
            kernel.Bind<ILogger>()
                .ToConstant(LogManager.GetCurrentClassLogger())
                .InTransientScope();

            //mass registration via assembly scanning
            kernel.Bind(t => t.FromAssemblyContaining<IRunner>()
                .SelectAllClasses()
                .BindDefaultInterfaces()
                .Configure(c => c.InTransientScope()));

            //resolve component root from container
            var runner = kernel.Get<IRunner>();
            runner.Run();
        }
    }
}
