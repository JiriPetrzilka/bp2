<Query Kind="Program" />

void Main()
{
//	var ogr = new ObjectGraphGenerator();
//	ogr.GenerateObjectGraph();
	
	var cgr = new CodeGenerator();
	cgr.GraphToObjects(@"C:\Users\Jiří Petržilka\Desktop\WebGraph.xml");
}

// Define other methods and classes here
class CodeGenerator
{
	#region consts

	string graphString = @"
<Item1>
  <Item2>
    <Item3>
      <Item4 />
      <Item5 />
      <Item6 />
    </Item3>
    <Item7>
      <Item8 />
      <Item9 />
      <Item10 />
      <Item11 />
    </Item7>
    <Item12>
      <Item13 />
      <Item14 />
      <Item15 />
      <Item16 />
    </Item12>
    <Item17>
      <Item18 />
      <Item19 />
      <Item20 />
      <Item21 />
      <Item22 />
    </Item17>
  </Item2>
  <Item23>
    <Item24>
      <Item25 />
      <Item26 />
      <Item27 />
      <Item28 />
    </Item24>
    <Item29 />
  </Item23>
</Item1>";

	string classPattern = @"public class {0} : I{0} 
	{{	
	{1}
	
	public {0}({2})
	{{
	{3}
	}}
	
	{4}";

	string classBody = @"public string PropertyA{get;set;}
		public int PropertyB{get;set;}
		
		public void MethodA()
		{
		
		}
		
		public void MethodB(int parameter1)
		{
		
		}
	}";

	string interfacePattern = "public interface I{0}\r\n{1}";
	string interfaceBody = @"{
		string PropertyA{get;set;}
		int PropertyB{get;set;}
		void MethodA();
		void MethodB(int parameter1);
	}";

	#endregion

	public void GraphToObjects(string xmlPath)
	{
		var graph = XElement.Load(xmlPath);

		var objectsToCreate = graph
		.DescendantsAndSelf()
		.Select(elm => new
		{
			Obj = elm,
			Dependencies = elm
				.Descendants()
				.Where(e => e.Parent == elm && e.HasElements)
		});

	var interfaces = objectsToCreate
		.Select(obj => String.Format(interfacePattern,
			obj.Obj.Name,
			interfaceBody));

	var classes = objectsToCreate
		.Select(obj => String.Format(classPattern,
		obj.Obj.Name,
		String.Join(Environment.NewLine, obj.Dependencies.Select(d => $"I{d.Name} _{d.Name};")),
		String.Join(", ", obj.Dependencies.Select(d => $"I{d.Name} _{d.Name}")),
		String.Join(Environment.NewLine, obj.Dependencies.Select(d => $"this._{d.Name} = _{d.Name};")),
		classBody));


	string output = String.Join(
		Environment.NewLine + Environment.NewLine,
		interfaces
			.Concat(classes))
			.Replace("_I", "i");

		output.Dump();

	var registrations = graph
		.DescendantsAndSelf()
		.Select(itm =>
			$"container.RegisterTransient<{itm.Name}, I{itm.Name}>();");
	String.Join(Environment.NewLine, registrations).Dump();
}
}

class ObjectGraphGenerator
{
	int maxObjectCount = 200;
	int maxDepth = 4;
	int maxObjectsPerClass = 6;
	int objectIndex = 1;
	Random rnd = new Random();

	public void GenerateObjectGraph()
	{
		XElement root = new XElement($"Item{objectIndex++.ToString()}");
		CreateSubobjects(root, 0);
		root.Dump();
	}
	void CreateSubobjects(XElement parent, int depth)
	{
		if (depth == maxDepth)
			return;

		for (int i = 0; i < rnd.Next(3, maxObjectsPerClass); i++)
		{
			if (objectIndex == maxObjectCount)
				break;

			var elm = new XElement($"Item{objectIndex++.ToString()}");
			parent.Add(elm);
			CreateSubobjects(elm, depth + 1);
		}
	}
}