﻿using System;
using System.Diagnostics;
using System.Timers;
using BenchmarkApp.Benchmarks.WebObjectGraph;
using BenchmarkApp.Containers;
using BenchmarkApp.Results;

namespace BenchmarkApp.Benchmarks
{
    public class WebObjectGraphBenchmark<TContainer> : BenchmarkBase<TContainer>
        where TContainer : IContainerAdapter, new()
    {
        public WebObjectGraphBenchmark(int runs) : base(runs)
        {
        }

        public override string BenchmarkName => "WebGraph";

        public override BenchmarkResults Run()
        {
            container = new TContainer();
            RegisterComponents();

            var results = new BenchmarkResults();
            results.BenchmarkName = BenchmarkName;
            results.ContainerName = container.ContainerName;

            var resolveTimeStopwatch = new Stopwatch();

            for (int run = 1; run <= Runs; run++)
            {
                resolveTimeStopwatch.Start();
                container.Resolve<IItem1>();
                resolveTimeStopwatch.Stop();

                var result = new ResultsItem();
                result.RunId = run;
                result.ResolveTimeMs = resolveTimeStopwatch.ElapsedMilliseconds;
                results.Results.Add(result);

                resolveTimeStopwatch.Reset();

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

            return results;
        }

        void RegisterComponents()
        {
            container.RegisterTransient<Item1, IItem1>();
            container.RegisterTransient<Item2, IItem2>();
            container.RegisterTransient<Item3, IItem3>();
            container.RegisterTransient<Item4, IItem4>();
            container.RegisterTransient<Item5, IItem5>();
            container.RegisterTransient<Item6, IItem6>();
            container.RegisterTransient<Item7, IItem7>();
            container.RegisterTransient<Item8, IItem8>();
            container.RegisterTransient<Item9, IItem9>();
            container.RegisterTransient<Item10, IItem10>();
            container.RegisterTransient<Item11, IItem11>();
            container.RegisterTransient<Item12, IItem12>();
            container.RegisterTransient<Item13, IItem13>();
            container.RegisterTransient<Item14, IItem14>();
            container.RegisterTransient<Item15, IItem15>();
            container.RegisterTransient<Item16, IItem16>();
            container.RegisterTransient<Item17, IItem17>();
            container.RegisterTransient<Item18, IItem18>();
            container.RegisterTransient<Item19, IItem19>();
            container.RegisterTransient<Item20, IItem20>();
            container.RegisterTransient<Item21, IItem21>();
            container.RegisterTransient<Item22, IItem22>();
            container.RegisterTransient<Item23, IItem23>();
            container.RegisterTransient<Item24, IItem24>();
            container.RegisterTransient<Item25, IItem25>();
            container.RegisterTransient<Item26, IItem26>();
            container.RegisterTransient<Item27, IItem27>();
            container.RegisterTransient<Item28, IItem28>();
            container.RegisterTransient<Item29, IItem29>();
            container.RegisterTransient<Item30, IItem30>();
            container.RegisterTransient<Item31, IItem31>();
            container.RegisterTransient<Item32, IItem32>();
            container.RegisterTransient<Item33, IItem33>();
            container.RegisterTransient<Item34, IItem34>();
            container.RegisterTransient<Item35, IItem35>();
            container.RegisterTransient<Item36, IItem36>();
            container.RegisterTransient<Item37, IItem37>();
            container.RegisterTransient<Item38, IItem38>();
            container.RegisterTransient<Item39, IItem39>();
            container.RegisterTransient<Item40, IItem40>();
            container.RegisterTransient<Item41, IItem41>();
            container.RegisterTransient<Item42, IItem42>();
            container.RegisterTransient<Item43, IItem43>();
            container.RegisterTransient<Item44, IItem44>();
            container.RegisterTransient<Item45, IItem45>();
            container.RegisterTransient<Item46, IItem46>();
            container.RegisterTransient<Item47, IItem47>();
            container.RegisterTransient<Item48, IItem48>();
            container.RegisterTransient<Item49, IItem49>();
            container.RegisterTransient<Item50, IItem50>();
            container.RegisterTransient<Item51, IItem51>();
            container.RegisterTransient<Item52, IItem52>();
            container.RegisterTransient<Item53, IItem53>();
            container.RegisterTransient<Item54, IItem54>();
            container.RegisterTransient<Item55, IItem55>();
            container.RegisterTransient<Item56, IItem56>();
            container.RegisterTransient<Item57, IItem57>();
            container.RegisterTransient<Item58, IItem58>();
            container.RegisterTransient<Item59, IItem59>();
            container.RegisterTransient<Item60, IItem60>();
            container.RegisterTransient<Item61, IItem61>();
            container.RegisterTransient<Item62, IItem62>();
            container.RegisterTransient<Item63, IItem63>();
            container.RegisterTransient<Item64, IItem64>();
            container.RegisterTransient<Item65, IItem65>();
            container.RegisterTransient<Item66, IItem66>();
            container.RegisterTransient<Item67, IItem67>();
            container.RegisterTransient<Item68, IItem68>();
            container.RegisterTransient<Item69, IItem69>();
            container.RegisterTransient<Item70, IItem70>();
            container.RegisterTransient<Item71, IItem71>();
            container.RegisterTransient<Item72, IItem72>();
            container.RegisterTransient<Item73, IItem73>();
            container.RegisterTransient<Item74, IItem74>();
            container.RegisterTransient<Item75, IItem75>();
            container.RegisterTransient<Item76, IItem76>();
            container.RegisterTransient<Item77, IItem77>();
            container.RegisterTransient<Item78, IItem78>();
            container.RegisterTransient<Item79, IItem79>();
            container.RegisterTransient<Item80, IItem80>();
            container.RegisterTransient<Item81, IItem81>();
            container.RegisterTransient<Item82, IItem82>();
            container.RegisterTransient<Item83, IItem83>();
            container.RegisterTransient<Item84, IItem84>();
            container.RegisterTransient<Item85, IItem85>();
            container.RegisterTransient<Item86, IItem86>();
            container.RegisterTransient<Item87, IItem87>();
            container.RegisterTransient<Item88, IItem88>();
            container.RegisterTransient<Item89, IItem89>();
            container.RegisterTransient<Item90, IItem90>();
            container.RegisterTransient<Item91, IItem91>();
            container.RegisterTransient<Item92, IItem92>();
            container.RegisterTransient<Item93, IItem93>();
            container.RegisterTransient<Item94, IItem94>();
            container.RegisterTransient<Item95, IItem95>();
            container.RegisterTransient<Item96, IItem96>();
            container.RegisterTransient<Item97, IItem97>();
            container.RegisterTransient<Item98, IItem98>();
            container.RegisterTransient<Item99, IItem99>();
            container.RegisterTransient<Item100, IItem100>();
            container.RegisterTransient<Item101, IItem101>();
            container.RegisterTransient<Item102, IItem102>();
            container.RegisterTransient<Item103, IItem103>();
            container.RegisterTransient<Item104, IItem104>();
            container.RegisterTransient<Item105, IItem105>();
            container.RegisterTransient<Item106, IItem106>();
            container.RegisterTransient<Item107, IItem107>();
            container.RegisterTransient<Item108, IItem108>();
            container.RegisterTransient<Item109, IItem109>();
            container.RegisterTransient<Item110, IItem110>();
            container.RegisterTransient<Item111, IItem111>();
            container.RegisterTransient<Item112, IItem112>();
            container.RegisterTransient<Item113, IItem113>();
            container.RegisterTransient<Item114, IItem114>();
            container.RegisterTransient<Item115, IItem115>();
            container.RegisterTransient<Item116, IItem116>();
            container.RegisterTransient<Item117, IItem117>();
            container.RegisterTransient<Item118, IItem118>();
            container.RegisterTransient<Item119, IItem119>();
            container.RegisterTransient<Item120, IItem120>();
            container.RegisterTransient<Item121, IItem121>();
            container.RegisterTransient<Item122, IItem122>();
            container.RegisterTransient<Item123, IItem123>();
            container.RegisterTransient<Item124, IItem124>();
            container.RegisterTransient<Item125, IItem125>();
            container.RegisterTransient<Item126, IItem126>();
            container.RegisterTransient<Item127, IItem127>();
            container.RegisterTransient<Item128, IItem128>();
            container.RegisterTransient<Item129, IItem129>();
            container.RegisterTransient<Item130, IItem130>();
            container.RegisterTransient<Item131, IItem131>();
            container.RegisterTransient<Item132, IItem132>();
            container.RegisterTransient<Item133, IItem133>();
            container.RegisterTransient<Item134, IItem134>();
            container.RegisterTransient<Item135, IItem135>();
            container.RegisterTransient<Item136, IItem136>();
            container.RegisterTransient<Item137, IItem137>();
            container.RegisterTransient<Item138, IItem138>();
            container.RegisterTransient<Item139, IItem139>();
            container.RegisterTransient<Item140, IItem140>();
            container.RegisterTransient<Item141, IItem141>();
            container.RegisterTransient<Item142, IItem142>();
            container.RegisterTransient<Item143, IItem143>();
            container.RegisterTransient<Item144, IItem144>();
            container.RegisterTransient<Item145, IItem145>();
            container.RegisterTransient<Item146, IItem146>();
            container.RegisterTransient<Item147, IItem147>();
            container.RegisterTransient<Item148, IItem148>();
            container.RegisterTransient<Item149, IItem149>();
            container.RegisterTransient<Item150, IItem150>();
            container.RegisterTransient<Item151, IItem151>();
            container.RegisterTransient<Item152, IItem152>();
            container.RegisterTransient<Item153, IItem153>();
            container.RegisterTransient<Item154, IItem154>();
            container.RegisterTransient<Item155, IItem155>();
            container.RegisterTransient<Item156, IItem156>();
            container.RegisterTransient<Item157, IItem157>();
            container.RegisterTransient<Item158, IItem158>();
            container.RegisterTransient<Item159, IItem159>();
            container.RegisterTransient<Item160, IItem160>();
            container.RegisterTransient<Item161, IItem161>();
            container.RegisterTransient<Item162, IItem162>();
            container.RegisterTransient<Item163, IItem163>();
            container.RegisterTransient<Item164, IItem164>();
            container.RegisterTransient<Item165, IItem165>();
            container.RegisterTransient<Item166, IItem166>();
            container.RegisterTransient<Item167, IItem167>();
            container.RegisterTransient<Item168, IItem168>();
            container.RegisterTransient<Item169, IItem169>();
            container.RegisterTransient<Item170, IItem170>();
            container.RegisterTransient<Item171, IItem171>();
            container.RegisterTransient<Item172, IItem172>();
            container.RegisterTransient<Item173, IItem173>();
            container.RegisterTransient<Item174, IItem174>();
            container.RegisterTransient<Item175, IItem175>();
            container.RegisterTransient<Item176, IItem176>();
            container.RegisterTransient<Item177, IItem177>();
            container.RegisterTransient<Item178, IItem178>();
            container.RegisterTransient<Item179, IItem179>();
            container.RegisterTransient<Item180, IItem180>();
            container.RegisterTransient<Item181, IItem181>();
            container.RegisterTransient<Item182, IItem182>();
            container.RegisterTransient<Item183, IItem183>();
            container.RegisterTransient<Item184, IItem184>();
            container.RegisterTransient<Item185, IItem185>();
            container.RegisterTransient<Item186, IItem186>();
            container.RegisterTransient<Item187, IItem187>();
            container.RegisterTransient<Item188, IItem188>();
            container.RegisterTransient<Item189, IItem189>();
            container.RegisterTransient<Item190, IItem190>();
            container.RegisterTransient<Item191, IItem191>();
            container.RegisterTransient<Item192, IItem192>();
            container.RegisterTransient<Item193, IItem193>();
            container.RegisterTransient<Item194, IItem194>();
            container.RegisterTransient<Item195, IItem195>();
            container.RegisterTransient<Item196, IItem196>();
            container.RegisterTransient<Item197, IItem197>();
            container.RegisterTransient<Item198, IItem198>();
            container.RegisterTransient<Item199, IItem199>();

            container.CloseContainerRegistrations();
        }
    }
}
