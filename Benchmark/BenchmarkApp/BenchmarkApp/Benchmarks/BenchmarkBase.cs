﻿using BenchmarkApp.Containers;
using BenchmarkApp.Results;

namespace BenchmarkApp.Benchmarks
{
    public abstract class BenchmarkBase<TContainer> 
        where TContainer : IContainerAdapter, new()
    {
        protected TContainer container;

        public BenchmarkBase(int runs) => Runs = runs;

        public abstract string BenchmarkName { get; }
        public int Runs { get; private set; }

        public abstract BenchmarkResults Run();
    }
}
