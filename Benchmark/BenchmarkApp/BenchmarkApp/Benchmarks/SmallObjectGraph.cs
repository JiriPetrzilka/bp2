﻿namespace BenchmarkApp.Benchmarks.SmallObjectGraph
{
    public interface IItem1
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem2
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem3
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem4
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem5
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem6
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem7
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem8
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem9
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem10
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem11
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem12
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem13
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem14
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem15
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem16
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem17
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem18
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem19
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem20
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem21
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem22
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem23
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem24
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem25
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem26
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem27
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem28
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public interface IItem29
    {
        string PropertyA { get; set; }
        int PropertyB { get; set; }
        void MethodA();
        void MethodB(int parameter1);
    }

    public class Item1 : IItem1
    {
        IItem2 item2;
        IItem23 item23;

        public Item1(IItem2 item2, IItem23 item23)
        {
            this.item2 = item2;
            this.item23 = item23;
        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item2 : IItem2
    {
        IItem3 item3;
        IItem7 item7;
        IItem12 item12;
        IItem17 item17;

        public Item2(IItem3 item3, IItem7 item7, IItem12 item12, IItem17 item17)
        {
            this.item3 = item3;
            this.item7 = item7;
            this.item12 = item12;
            this.item17 = item17;
        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item3 : IItem3
    {


        public Item3()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item4 : IItem4
    {


        public Item4()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item5 : IItem5
    {


        public Item5()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item6 : IItem6
    {


        public Item6()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item7 : IItem7
    {


        public Item7()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item8 : IItem8
    {


        public Item8()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item9 : IItem9
    {


        public Item9()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item10 : IItem10
    {


        public Item10()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item11 : IItem11
    {


        public Item11()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item12 : IItem12
    {


        public Item12()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item13 : IItem13
    {


        public Item13()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item14 : IItem14
    {


        public Item14()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item15 : IItem15
    {


        public Item15()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item16 : IItem16
    {


        public Item16()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item17 : IItem17
    {


        public Item17()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item18 : IItem18
    {


        public Item18()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item19 : IItem19
    {


        public Item19()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item20 : IItem20
    {


        public Item20()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item21 : IItem21
    {


        public Item21()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item22 : IItem22
    {


        public Item22()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item23 : IItem23
    {
        IItem24 item24;

        public Item23(IItem24 item24)
        {
            this.item24 = item24;
        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item24 : IItem24
    {


        public Item24()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item25 : IItem25
    {


        public Item25()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item26 : IItem26
    {


        public Item26()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item27 : IItem27
    {


        public Item27()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item28 : IItem28
    {


        public Item28()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }

    public class Item29 : IItem29
    {


        public Item29()
        {

        }

        public string PropertyA { get; set; }
        public int PropertyB { get; set; }

        public void MethodA()
        {

        }

        public void MethodB(int parameter1)
        {

        }
    }


}
