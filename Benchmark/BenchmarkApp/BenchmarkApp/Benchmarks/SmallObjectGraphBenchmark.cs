﻿using System;
using System.Diagnostics;
using BenchmarkApp.Benchmarks.SmallObjectGraph;
using BenchmarkApp.Containers;
using BenchmarkApp.Results;

namespace BenchmarkApp.Benchmarks
{
    public class SmallObjectGraphBenchmark<TContainer> : BenchmarkBase<TContainer>
        where TContainer : IContainerAdapter, new()
    {
        public SmallObjectGraphBenchmark(int runs) : base(runs)
        {
        }

        public override string BenchmarkName => "SmallObjectGraph";

        public override BenchmarkResults Run()
        {
            var results = new BenchmarkResults();
            results.BenchmarkName = BenchmarkName;
            Stopwatch totalTimeStopwatch = new Stopwatch();
            Stopwatch resolveTimeStopwatch = new Stopwatch();

            for (int run = 1; run <= Runs; run++)
            {
                totalTimeStopwatch.Start();
                container = new TContainer();
                results.ContainerName = container.ContainerName;
                RegisterComponents();

                resolveTimeStopwatch.Start();
                container.Resolve<IItem1>();
                resolveTimeStopwatch.Stop();
                totalTimeStopwatch.Stop();

                var result = new ResultsItem();
                result.RunId = run;
                result.ResolveTimeMs = resolveTimeStopwatch.ElapsedMilliseconds;
                result.TotalTimeMs = totalTimeStopwatch.ElapsedMilliseconds;
                results.Results.Add(result);

                resolveTimeStopwatch.Reset();
                totalTimeStopwatch.Reset();

                container.Dispose();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

            return results;
        }

        void RegisterComponents()
        {
            container.RegisterTransient<Item1, IItem1>();
            container.RegisterTransient<Item2, IItem2>();
            container.RegisterTransient<Item3, IItem3>();
            container.RegisterTransient<Item4, IItem4>();
            container.RegisterTransient<Item5, IItem5>();
            container.RegisterTransient<Item6, IItem6>();
            container.RegisterTransient<Item7, IItem7>();
            container.RegisterTransient<Item8, IItem8>();
            container.RegisterTransient<Item9, IItem9>();
            container.RegisterTransient<Item10, IItem10>();
            container.RegisterTransient<Item11, IItem11>();
            container.RegisterTransient<Item12, IItem12>();
            container.RegisterTransient<Item13, IItem13>();
            container.RegisterTransient<Item14, IItem14>();
            container.RegisterTransient<Item15, IItem15>();
            container.RegisterTransient<Item16, IItem16>();
            container.RegisterTransient<Item17, IItem17>();
            container.RegisterTransient<Item18, IItem18>();
            container.RegisterTransient<Item19, IItem19>();
            container.RegisterTransient<Item20, IItem20>();
            container.RegisterTransient<Item21, IItem21>();
            container.RegisterTransient<Item22, IItem22>();
            container.RegisterTransient<Item23, IItem23>();
            container.RegisterTransient<Item24, IItem24>();
            container.RegisterTransient<Item25, IItem25>();
            container.RegisterTransient<Item26, IItem26>();
            container.RegisterTransient<Item27, IItem27>();
            container.RegisterTransient<Item28, IItem28>();
            container.RegisterTransient<Item29, IItem29>();

            container.CloseContainerRegistrations();
        }
    }
}
