﻿using System;
using SimpleInjector;

namespace BenchmarkApp.Containers
{
    public class SimpleInjectorAdapter : IContainerAdapter
    {
        Container container;

        public SimpleInjectorAdapter() => container = new Container();

        public string ContainerName => "SimpleInjector";

        public void RegisterSingleton<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => container.Register<TService, TComponent>(Lifestyle.Singleton);
                  
        public void RegisterTransient<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => container.Register<TService, TComponent>(Lifestyle.Transient);

        public void CloseContainerRegistrations() => container.Verify();

        public TService Resolve<TService>()
            where TService : class
            => container.GetInstance<TService>();

        public void Dispose() => container.Dispose();
    }
}
