﻿namespace BenchmarkApp.Containers
{
    public interface IContainerAdapter
    {
        string ContainerName { get; }
        void CloseContainerRegistrations();

        TService Resolve<TService>()
            where TService : class;

        void RegisterSingleton<TComponent, TService>()
            where TComponent : class, TService
            where TService : class;

        void RegisterTransient<TComponent, TService>() 
            where TComponent : class, TService
            where TService : class;

        void Dispose();
    }
}
