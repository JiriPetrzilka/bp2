﻿using Autofac;

namespace BenchmarkApp.Containers
{
    public class AutofacAdapter : IContainerAdapter
    {
        ContainerBuilder containerBuilder;
        IContainer container;

        public AutofacAdapter()
            => containerBuilder = new ContainerBuilder();

        public string ContainerName => "Autofac";

        public void CloseContainerRegistrations()
            => container = containerBuilder.Build();

        public void RegisterSingleton<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => containerBuilder
                    .RegisterType<TComponent>()
                    .As<TService>()
                    .SingleInstance();

        public void RegisterTransient<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => containerBuilder
                    .RegisterType<TComponent>()
                    .As<TService>()
                    .InstancePerDependency();

        public TService Resolve<TService>()
            where TService : class
            => container.Resolve<TService>();

        public void Dispose() => container.Dispose();
    }
}
