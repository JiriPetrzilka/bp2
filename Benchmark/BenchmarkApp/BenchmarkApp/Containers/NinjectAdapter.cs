﻿using Ninject;

namespace BenchmarkApp.Containers
{
    public class NinjectAdapter : IContainerAdapter
    {
        StandardKernel container;

        public NinjectAdapter()
            => container = new StandardKernel();

        public string ContainerName => "Ninject";

        public void CloseContainerRegistrations()
        {}

        public void RegisterSingleton<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => container
                   .Bind<TService>()
                   .To<TComponent>()
                   .InSingletonScope();

        public void RegisterTransient<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => container
                   .Bind<TService>()
                   .To<TComponent>()
                   .InTransientScope();

        public TService Resolve<TService>()
            where TService : class
            => container.Get<TService>();

        public void Dispose() => container.Dispose();
    }
}
