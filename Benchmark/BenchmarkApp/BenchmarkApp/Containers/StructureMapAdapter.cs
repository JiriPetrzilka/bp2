﻿using System;
using System.Collections.Generic;
using StructureMap;

namespace BenchmarkApp.Containers
{
    public class StructureMapAdapter : IContainerAdapter
    {
        List<Action<ConfigurationExpression>> registrations;
        Container container;

        public StructureMapAdapter()
            => registrations = new List<Action<ConfigurationExpression>>();

        public string ContainerName => "StructureMap";

        public void CloseContainerRegistrations()
            => container = new Container(config =>
               registrations.ForEach(reg => reg(config)));

        public void RegisterSingleton<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => registrations.Add(c => c.For<TService>()
                   .Use<TComponent>()
                   .Singleton());

        public void RegisterTransient<TComponent, TService>()
            where TComponent : class, TService
            where TService : class            
            => registrations.Add(c => c.For<TService>()
                   .Use<TComponent>()
                   .Transient());

        public TService Resolve<TService>()
            where TService : class
            => container.GetInstance<TService>();

        public void Dispose() => container.Dispose();
    }
}
