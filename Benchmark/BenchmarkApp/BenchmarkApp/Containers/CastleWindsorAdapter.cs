﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace BenchmarkApp.Containers
{
    public class CastleWindsorAdapter : IContainerAdapter
    {
        WindsorContainer container;

        public CastleWindsorAdapter()
            => container = new WindsorContainer();

        public string ContainerName => "Castle Windsor";

        public void CloseContainerRegistrations()
        { }

        public void RegisterSingleton<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => container.Register(Component
                .For<TService>()
                .ImplementedBy<TComponent>()
                .LifestyleSingleton());

        public void RegisterTransient<TComponent, TService>()
            where TComponent : class, TService
            where TService : class
            => container.Register(Component
                .For<TService>()
                .ImplementedBy<TComponent>()
                .LifestyleTransient());

        public TService Resolve<TService>()
            where TService : class
            => container.Resolve<TService>();

        public void Dispose() => container.Dispose();
    }
}
