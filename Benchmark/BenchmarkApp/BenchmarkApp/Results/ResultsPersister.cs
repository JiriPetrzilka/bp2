﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace BenchmarkApp.Results
{
    public class ResultsPersister
    {
        public void SaveResultsJson(BenchmarkResults results)
        {
            var fileName = GetFileName(results.BenchmarkName, results.ContainerName, "json");
            var data = JsonConvert.SerializeObject(results, Formatting.Indented);
            File.WriteAllText(fileName, data);
        }

        public void SaveResultsCsv(BenchmarkResults results)
        {
            var fileName = GetFileName(results.BenchmarkName, results.ContainerName, "csv");

            using (var writer = new StreamWriter(fileName))
            {
                var csv = new CsvHelper.CsvWriter(writer);
                csv.WriteRecords(results.Results);
            }
        }

        public string GetFileName(
            string benchmarkName,
            string containerName,
            string extension)
        {
            string fileName = String.Format("{0}_{1}_{2:yyyyMMddHHmmss}.{3}",
                benchmarkName,
                containerName,
                DateTime.Now,
                extension);
            string path = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                fileName);

            return path;
        }
    }
}
