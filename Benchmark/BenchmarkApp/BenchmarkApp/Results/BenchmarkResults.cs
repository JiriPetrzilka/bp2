﻿using System.Collections.Generic;

namespace BenchmarkApp.Results
{
    public class BenchmarkResults
    {
        public BenchmarkResults() => Results = new List<ResultsItem>();

        public string BenchmarkName { get; set; }
        public string ContainerName { get; set; }

        public ResultsStats StartTimeStats
            => new ResultsStats(Results, x => x.ContainerStartTimeMs);
        public ResultsStats ResolveTimeStats
            => new ResultsStats(Results, x => x.ResolveTimeMs);
        public ResultsStats TotalTimeStats
            => new ResultsStats(Results, x => x.TotalTimeMs);

        public List<ResultsItem> Results { get; set; }
    }
}
