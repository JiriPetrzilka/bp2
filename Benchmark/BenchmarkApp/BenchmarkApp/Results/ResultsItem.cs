﻿namespace BenchmarkApp.Results
{
    public class ResultsItem
    {
        public int RunId { get; set; }
        public long ResolveTimeMs { get; set; }
        public long TotalTimeMs { get; set; }
        public long ContainerStartTimeMs => TotalTimeMs - ResolveTimeMs;
    }
}
