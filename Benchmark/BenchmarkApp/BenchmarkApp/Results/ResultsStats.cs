﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BenchmarkApp.Results
{
    public class ResultsStats
    {
        IEnumerable<ResultsItem> results;
        Func<ResultsItem, long> selector;

        public ResultsStats(
            IEnumerable<ResultsItem> results,
            Func<ResultsItem, long> selector)
        {
            this.results = results;
            this.selector = selector;
        }

        public long Min => results.Min(selector);
        public long Max => results.Max(selector);
        public double Avg => results.Average(selector);

        public double Sd
        {
            get
            {
                double avg = Avg;
                double sum = results.Sum(x =>
                    Math.Pow(Convert.ToDouble(selector(x)) - avg, 2));
                double count = Convert.ToDouble(results.Count()) - 1;

                return Math.Sqrt(sum / count);
            }
        }
    }
}
