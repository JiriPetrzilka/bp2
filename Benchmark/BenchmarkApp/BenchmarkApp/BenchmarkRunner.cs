﻿using System;
using BenchmarkApp.Benchmarks;
using BenchmarkApp.Containers;
using BenchmarkApp.Results;

namespace BenchmarkApp
{
    public class BenchmarkRunner
    {
        public int RunBenchmark(string benchmark, int runs)
        {
            var persister = new ResultsPersister();
            persister.SaveResultsJson(CreateBenchmark<CastleWindsorAdapter>(benchmark, runs).Run());
            persister.SaveResultsJson(CreateBenchmark<AutofacAdapter>(benchmark, runs).Run());
            persister.SaveResultsJson(CreateBenchmark<SimpleInjectorAdapter>(benchmark, runs).Run());
            persister.SaveResultsJson(CreateBenchmark<NinjectAdapter>(benchmark, runs).Run());
            persister.SaveResultsJson(CreateBenchmark<StructureMapAdapter>(benchmark, runs).Run());

            return 0;
        }

        BenchmarkBase<TContainer> CreateBenchmark<TContainer>(string benchmarkName, int runs)
            where TContainer : IContainerAdapter, new()
        {
            switch (benchmarkName)
            {
                case "SmallObjectGraph":
                case "sog":
                    return new SmallObjectGraphBenchmark<TContainer>(runs);
                case "LargeObjectGraph":
                case "log":
                    return new LargeObjectGraphBenchmark<TContainer>(runs);
                case "WebObjectGraph":
                case "wog":
                    return new WebObjectGraphBenchmark<TContainer>(runs);
                default:
                    throw new NotSupportedException(benchmarkName ?? String.Empty);
            }
        }
    }
}
