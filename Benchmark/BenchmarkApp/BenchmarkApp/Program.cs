﻿using System;
using BenchmarkApp.Benchmarks;
using BenchmarkApp.Containers;
using BenchmarkApp.Results;
using Microsoft.Extensions.CommandLineUtils;

namespace BenchmarkApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test();
            var app = new CommandLineApplication();
            var benchOpt = app.Option("-b|--benchmark <benchmark>",
                "The benchmark to be used",
                CommandOptionType.SingleValue);
            var runsOpt = app.Option("-r|--runs <runs>",
                "Number of times for the benchmark to be run",
                CommandOptionType.SingleValue);
            var runner = new BenchmarkRunner();
            app.OnExecute(() => runner.RunBenchmark(
                benchOpt.Value(),
                Int32.Parse(runsOpt.Value())));
            app.Execute(args);
        }

        static void Test()
        {
            int runs = 1000;
            var bench = new SmallObjectGraphBenchmark<AutofacAdapter>(runs);
            var results = bench.Run();

            var persister = new ResultsPersister();
            persister.SaveResultsCsv(results);
        }
    }
}
